const Bundle = require('bono');
const AppBundle = require('./app.js');
// const NormBundle = require('bono-norm/bundle');

module.exports = class Api extends Bundle {
  constructor ({ manager, secret, logger }) {
  // constructor ({ manager, secret }) {
    super();

    this.manager = manager;

    this.use(require('kcors')());
    this.use(require('bono/middlewares/json')());
    // this.use(require('koa-jwt')({ secret }));
    this.use(require('bono-norm/middleware')({ manager }));

    this.use(async (ctx, next) => {
      ctx.state.secret = secret;
      ctx.state.logger = logger;
      await next();
    });
    this.bundle('/example', new AppBundle({ schema: 'example' }));
    this.bundle('/todolist', new AppBundle({ schema: 'todolist' }));
    this.get('/', this.index.bind(this));
  }

  index (ctx) {
    const { name, version } = require('../package.json');
    return { name, version };
  }
};
