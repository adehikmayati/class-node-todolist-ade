const NString = require('node-norm/schemas/nstring');
const Actorable = require('node-norm/observers/actorable');
const Timestampable = require('node-norm/observers/timestampable');

module.exports = {
  name: 'todolist',
  fields: [
    new NString('list'),
  ],
  observers: [
    new Actorable(),
    new Timestampable(),
  ],
};
