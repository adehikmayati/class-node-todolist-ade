const NString = require('node-norm/schemas/nstring');
const Actorable = require('node-norm/observers/actorable');
const Timestampable = require('node-norm/observers/timestampable');

module.exports = {
  name: 'example',
  fields: [
    new NString('name'),
    new NString('nik'),
  ],
  observers: [
    new Actorable(),
    new Timestampable(),
  ],
};
