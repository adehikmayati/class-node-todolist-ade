const Bundle = require('bono');
const Api = require('./bundles/api');

const { Manager } = require('node-norm');
// const Excel = require('./bundles/export');

class App extends Bundle {
  constructor ({ connections, secret, logger, urlNotification }) {
    super();

    const manager = new Manager({ connections });

    this.use(require('kcors')());

    this.bundle('/api', new Api({ manager, secret, logger }));

    this.get('/', this.index.bind(this));
  }

  index (ctx) {
    const { name, version } = require('./package.json');
    ctx.body = { name, version };
  }
}

module.exports = App;
