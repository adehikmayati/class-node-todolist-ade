const { Manager } = require('node-norm');
const config = require('../config');

module.exports = {
  async up () {
    const manager = new Manager(config);
    try {
      await manager.runSession(async (session) => {
        await session.factory('todolist').define();
      });
    } finally {
      await manager.end();
    }
  },

  async down () {
    const manager = new Manager(config);
    try {
      await manager.runSession(async (session) => {
        await session.factory('todolist').undefine();
      });
    } finally {
      await manager.end();
    }
  },
};
