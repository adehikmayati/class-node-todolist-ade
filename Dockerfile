FROM node:12-alpine

ARG ENV
RUN apk add --no-cache chromium
WORKDIR /app
COPY ./package.json ./package-lock.json /app/
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
RUN set -x \
  && echo "ENV=${ENV}" \
  && if [ "${ENV}" != "development" ]; then \
    npm i --only=production; \
    npm cache clean --force; \
  fi
COPY . /app

CMD [ "node", "server.js" ]
