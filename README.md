# Node Bono
Sebuah Skeleton Framework yang dikembangkan oleh Xinix Technology guna pembuatan aplikasi Back-End. Node Bono dibangun dengan NodeJS, Bono dan Norm.

# Isi Konten
- [Prasyarat](#prasyarat)
- [Instalasi](#instalasi)
- [Eksekusi](#eksekusi)
- [Struktur Aplikasi](#struktur-aplikasi)
- [Aturan Table Database](#aturan-table-database)
- [Schemas](#schemas)
- [Bundles](#bundles)
- [Migrations](#migrations)

# Prasyarat
Adapun perangkat lunak yang dibutuhkan untuk menjalankan bono secara optimal adalah sebagai berikut:
- Git - Untuk mengakses Source Code Repository
- Docker - Untuk menjalankan server sebagai container
- Docker Compose - Untuk config Docker
- NodeJS >= 8.9.0 - Untuk menjalankan server, testing, dan generate distributed distributed
- Mocha - Untuk menjalankan testing. Diharuskan untuk menginstall mocha secara global.
- Postman - Untuk API Testing
```shell
$ npm install -g mocha
```
- ESLint - Untuk analisis code. Diharuskan untuk menginstall ESLint dan depedensi nya secara global
```shell
npm install eslint eslint-config-standard eslint-config-xinix eslint-plugin-import eslint-plugin-node eslint-plugin-promise eslint-plugin-standard babel-eslint -g
```
- Nodemon - untuk menjalankan server saat development. Diharuskan menginstall Nodemon secara global
```shell
npm install -g nodemon
```

# Instalasi
Untuk menginstall Node Bono, Demi Keaman Anda diharuskan untuk melakukan forking source code dari [Repository Node Bono](https://gitlab.com/sagara-xinix/framework/nodejs-backend) ke dalam akun gitlab anda. Kemudian, anda bisa menjalankan perintah berikut ini pada terminal anda.
```shell
$ git clone git@gitlab.com:username/nodejs-backend.git
$ cd nodejs-backend
$ npm install
``` 

# Eksekusi
Sebelum mengeksekusi Node Bono, Anda diharuskan untuk membuat sebuah database misal dengan nama `example` dan mengkonfigurasi `username` dan `password` dari MySQL anda. Setelah itu Anda dapat menjalankan perintah berikut ini
```shell
$ NORM_ADAPTER=mysql NORM_MYSQL_HOST=localhost NORM_MYSQL_USER=root NORM_MYSQL_PASSWORD=password NORM_MYSQL_DATABASE=example  npm run dev
```
Atau jika anda menggunakan Sistem Operasi Windows, Anda bisa melakukan perintah berikut pada terminal dan jalankan setiap anda ingin memulai awal pengembangan karena pada Windows Environtment Variable tersebut sifatnya Temporary.
```shell
$ set NORM_ADAPTER=mysql
$ set NORM_MYSQL_HOST=localhost
$ set NORM_MYSQL_USER=root
$ set NORM_MYSQL_PASSWORD=password
$ set NORM_MYSQL_DATABASE=example
$ npm run dev
```
Maka Nodemon akan menjalankan file `server.js` dan berjalan pada alamat `http://localhost:3000`. Alamat tersebut bisa dikunjungi via Web Browser ataupun Postman

# Struktur Aplikasi
Adapun struktur direktori inti dari aplikasi Node Bono adalah sebagai berikut:
- `bin` - Direktori untuk menyimpan *binary* atau *executable files*
- `bundles` - Direktori untuk menyimpan aplikasi Bundles
- `docs` - Direktori untuk menyimpan dokumentasi
- `migrations` - Direktori untuk menyimpan berkas Migrasi
- `schemas` - Direktori untuk menyimpan Schema
- `test` - Direktori yang digunakan untuk testing aplikasi
- `config.js` - Berkas yang digunakan untuk konfigurasi *Environtment Variables* Node Bono

# Aturan Table Database
Dalam membuat *table* baru di database, ada beberapa field yang wajib dibuat agar proses CRUD berjalan dengan baik, field tersebut diantaranya adalah
| Nama Field   | Tipe Data | Default     | Deskripsi                                   |
| ------------ | --------- | ----------- | ------------------------------------------- |
| created_by   | VARCHAR   | NULL        | Menyimpan ID User yang membuat data.        |
| updated_by   | VARCHAR   | NULL        | Menyimpan ID User yang merubah data.        |
| created_time | VARCHAR   | NULL        | Menyimpan informasi waktu kala data dibuat. |
| updated_time | VARCHAR   | NULL        | Menyimpan informasi waktu kala data dirubah.|

field `created_by` dan `updated_by` akan di *generate* otomatis oleh module `node-norm/observers/actorable` dan mengisi kolom tersebut apabila aplikasinya mengimplementasikan JWT (JSON Web Token) sebagai mekanisme autentikasi. Sementara field `created_time` dan `updated_time` akan di *generate* otomatis oleh module `node-norm/observers/timestampable`. Dua modules tersebut merupakan `Observers` akan digunakan apabila Anda membuat Schemas.

# Schemas
Schemas merupakan representasi dari table dalam database. Dalam Schema, Anda dapat mendefinisikan nama field dan tipe data sesuai dengan apa yang ada di dalam table database. Node Bono menyediakan modules bernama Node Norm yang siap pakai guna mereprentasikan tipe data dari field / column yang ada di database anda. Modules ini terletak di `node-norm/schemas`. Modules Node Norm yang dapat antara lain sebagai berikut

| Nama Modules   | Deskripsi                            |
| -------------- | ------------------------------------ |
| `nbig.js`      | Digunakan untuk tipe data BigInteger |
| `nboolean.js`  | Digunakan untuk tipe data Boolean    |
| `ndatetime.js` | Digunakan untuk tipe data DateTime   |
| `ndouble.js`   | Digunakan untuk tipe data Double     |
| `ninteger.js`  | Digunakan untuk tipe data Integer    |
| `nstring.js`   | Digunakan untuk tipe data Varchar    |
| `ntext.js`     | Digunakan untuk tipe data Text       |

## Isi Schema
Karena schema merupakan representasi dari table, maka umumnya nama dari schema sendiri sama dengan nama table yang ada di database. Misal, anda memiliki sebuah table bernama movies di database, maka nama schema yang disarankan adalah `movies.js`. Adapun isi dari schemas pada umumnya adalah seperti ini:
```javascript
const NString = require('node-norm/schemas/nstring');
const NText = require('node-norm/schemas/ntext')
const Actorable = require('node-norm/observers/actorable');
const Timestampable = require('node-norm/observers/timestampable');

module.exports = {
  name: 'movies',
  fields: [
    new NString('title'),
    new NString('genre'),
    new NText('synopsis'),
    new NString('release_year'),
  ],
  observers: [
    new Actorable(),
    new Timestampable(),
  ],
};
```
> ***Penjelasan*** : Disini kita meng-export schema diatas bernama movies yang fields nya terdiri dari title berupa VARCHAR, genre berupa VARCHAR, synopsis berupa TEXT, dan release_year berupa VARCHAR. Kemudian setelah mendeklarasikan field nya, kita juga mendeklarasikan observers nya yakni Actorable() dan Timestampable(). Observers ini untuk mengisi created_by, updated_by, created_time, dan updated_time
Isi schema ini juga nantinya akan berpengaruh terhadap struktur dari sebuah table pada database jika kita menjalankan migrations pada Node Bono.

# Bundles
Bundles merupakan sebuah fitur dalam Node Bono yang berisi kumpulan module siap pakai yang dibungkus menjadi satu module saja. Dalam Node Bono terdapat default bundles yang terletak pada direktori bundles yaitu `Api.js` dan `App.js`

Bundles `App.js` sudah menyediakan semua kebutuhan untuk operasi CRUD dan pada `Api.js` kita bisa membuat sebuah alamat URI sebagai endpoint untuk mengakses schema tertentu. Caranya pada `Api.js` kita deklarasikan seperti berikut:
```javascript
this.bundle('/example', new AppBundle({ schema: 'example' }));
```
> ***Penjelasan*** : instance dari AppBundle tersebut nantinya akan membuat alamat `localhost:3000/api/example` yang nantinya akan mengakses schema `example.js` yang berada pada direktori schemas. URI ini juga akan merujuk ke table example yang berada pada database. 

## Membuat Custom Bundles
Selain dapat memanfaatkan AppBundle, apabila anda ingin membuat custom function dan custom URI, Node Bono memberikan keleluasaan untuk membuat Custom Bundles. Untuk membuat Custom Bundles, cukup dengan membuat file baru pada direktori bundles, misal dengan nama `movie.js`. yang isinya seperti berikut ini:
```javascript
const AppBundle = require('./app');

module.exports = class Movie extends AppBundle {
  constructor ({ schema }) {
    super({ schema });

    this.get('/test/samplemovie', this.samplemovie.bind(this));
  }

  samplemovie (ctx, exp = false) {
    console.log("This is function Sample Movie from movie.js Bundle");
  }

};
```
> ***Penjelasan*** : Pada snippets di atas, class Movie menjadi kelas turunan dari AppBundle dan pada class tersebut di deklarasikan sebuah URI sebagai contoh yakni `test/samplemovie` yang mengakses dan menjalankan function samplemovie( )

Setelah membuat custom bundles tersebut, anda harus mendaftarkan custom bundles tersebut pada class Api di `Api.js` seperti berikut ini:
```javascript
this.bundle('/movies', new MovieBundle({schema: 'movies'}));
```

Dari snippets diatas, maka akan memiliki URI yakni `localhost:3000/movies` yang didalamnya termasuk routing untuk melakukan CRUD dan `localhost:3000/api/movies/test/samplemovie` yang berasal dari function `samplemovie( )` yang sudah dibuat pada `bundles/movie.js`.

# Resource Routing
Resource Routing merupakan sebuah Fitur Routing dalam Node Bono yang dapat menangani kebutuhan untuk Create, Read, Update dan Delete (CRUD). Misal kita memiliki endpoint yakni `/movies`, maka berikut merupakan *action* yang di handle oleh Resource Routing
| HTTP Method   | URI          | Action     | Deskripsi                                                       |
| ------------- | ------------ | ---------- | --------------------------------------------------------------- |
| GET           | /movies      | index      | Menampilkan semua data pada schemas / table movies              |
| POST          | /movies      | create     | Membuat dan menyimpan data baru ke dalam schemas / table movies |
| GET           | /movies/{id} | read       | Menampilkan data berdasarkan ID                                 |
| PUT           | /movies/{id} | update     | Update data berdasarkan ID                                      |
| DELETE        | /movies/{id} | delete     | Menghapus data berdasarkan ID                                   |

# Migrations
Migrations merupakan fitur dalam Node Bono dimana mengizinkan Anda untuk membuat skema tabel pada database dan di generasi secara otomatis berdasarkan schemas yang anda buat, alih-alih menggunakan cara yang manual. Struktur dari migrations memiliki dua method utama, yakni `up` dan `down`. Fungsi dari method `up` yakni membuat atau menambah tabel dan atau column ke dalam database Anda. Sementara method `down` melakukan operasi kebalikan dari method `up`.

## Membuat Custom Migrations
Anda dengan mudah membuat Custom Migrations untuk kebutuhan tertentu, yakni dengan cara membuat berkas baru di direktori `/migrations`. Untuk aturan penamaan tidak ada standar yang khusus, namun umumnya struktur nama dari file migration adalah "tahunbulantanggal_konteks_migrasi". Sebagai contoh jika Anda ingin membuat tables Movies maka penamaan filenya adalah `20220301_create_movies_table.js`

Adapun isi dari file Migrations pada umumnya berisi seperti ini:
```javascript
const { Manager } = require('node-norm');
const config = require('../config');

module.exports = {
  async up () {
    const manager = new Manager(config);
    try {
      await manager.runSession(async (session) => {
        await session.factory('nama_schemas').define();
      });
    } finally {
      await manager.end();
    }
  },

  async down () {
    const manager = new Manager(config);
    try {
      await manager.runSession(async (session) => {
        await session.factory('nama_schemas').undefine();
      });
    } finally {
      await manager.end();
    }
  },
};

```

## Menjalankan Migrations
Untuk menjalankan migrations, pastikan Anda sudah membuat schemas nya terlebih dahulu yang didalamnya memiliki nama dan tipe data yang sesuai. Ada beberapa command yang bisa anda pakai untuk menjalankan migrations, yakni:
```shell
$ npm run migrate up
```
> ***Penjelasan*** : Command ini digunakan untuk menjankan semua file Migrations

```shell
$ npm run migrate down
```
> ***Penjelasan*** : Command ini digunakan untuk mengembalikan aksi yang dilakukan oleh migrate up

```shell
$ npm run migrate status
```
> ***Penjelasan*** : Command ini digunakan untuk melihat status migrasi, apa saja yang sudah tereksekusi, pending dan saat ini bisa di eksekusi.

```shell
$ npm run migrate next
```
> ***Penjelasan*** : Command ini digunakan untuk menjalankan aksi migrate untuk satu file migrasi

```shell
$ npm run migrate next
```
> ***Penjelasan*** : Command ini digunakan untuk mengembalikan aksi migrate hanyak untuk satu file migrasi saja.
